/* $FILENAME$ day_two.c */
/*  This is a multi-line comment.  They start with slash-star and end with
 *  star slash.  The * at the start of this line is just for looks.
 */

/* It works on a single line as well. */

int main(int argc, char **argv)
{
    return 0;
}

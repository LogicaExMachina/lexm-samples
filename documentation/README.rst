==================================
README for LogicaExMachina samples
==================================

----------
More Text?
----------

.. contents:: Table of Contents

How To Use This Project
=======================

A Quick Overview
----------------

This project exists to provide an easily accessible version of the code used
by LogicaExMachina_. This code in this project should function in a majority
of environments with a minimum of fuss.


Layout
------

The project is laid out in a simple manner:

- documentation:

  Contains general documentation regarding the project

- projects:

  Contains the source code used by LogicaExMachina_ organized in directories:

  * languages:
    
    Holds the source code organized by language. The code is further grouped
    by directories, one for each day.

    + day_2
    + day_3

  * multi
    Contains source code for any multi-language project.
    
    


.. _LogicaExMachina: http://www.logicaexmachina.net/


